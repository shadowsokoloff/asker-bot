from flask import Flask
from flask import request
import requests
import config
from PyTele.tele_entities import Bot
from PyTele.incoming_message_processor import Processor
import uuid
import yaml
from logging.config import dictConfig


with open('logconfig.yml', 'r') as f:
    config = yaml.safe_load(f.read())
    dictConfig(config)


app = Flask(__name__)




# метод для тестирования работы uwsgi
@app.route("/")
def hello_world():
    return "It's working"


# роут для принятия входяхих сообщений
@app.route("/", methods=["POST"])
def incoming_message():
    update = request.get_json()
    uid = '[{}]'.format(str(uuid.uuid4())[0:8])
    app.logger.debug('[INCOMMING_MESSAGE]: {}'.format(update), extra={'uid': uid})
    Bot(update=update, uid=uid)

    return 'OK'


# метод для генерации url
def get_url(method):
    return "https://api.telegram.org/bot{}/{}".format(config.token, method)


# роут задания вебхука
@app.route("/setwebhook")
def set_webhook():
    body = {'url': 'https://212.237.32.238'}
    files = {'certificate': ('tele.pem', open('/etc/ssl/telegram/tele.pem', 'r'), 'multipart/form-data')}
    r = requests.post(get_url('setwebhook'), body, files=files)
    r = requests.get(get_url("getWebHookInfo"))
    response = str(r.json())
    return response


# роут получения данных о текущем вебхуке
@app.route("/getwebhook")
def get_webhook():
    r = requests.get(get_url("getWebhookInfo"))
    response = str(r.json())
    return response


# роут удаления текущего вебхука
@app.route("/deletewebhook")
def deletewebhook():
    r = requests.get(get_url("deletewebhook"))
    response = str(r.json())
    return response
