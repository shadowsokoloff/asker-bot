from PyTele.tele_entities import User, Chat, Update, Message, CallbackQuery

from flask import current_app




class Processor:
    def __init__(update):
        """deprecated"""
        if 'message' in update.keys() and 'new_chat_members' in update['message'].keys():
            chat = Chat(update['message'])
            message = Message(update['message'])
            user = User(update['message']['new_chat_members'][0], chat=chat, message=message)
            # user.send_hello()
            # user.restrict()
        elif 'callback_query' in update.keys():
            chat = Chat(update['callback_query']['message'])
            message = Message(update['callback_query']['message'])
            callback = CallbackQuery(update, chat, message)
            current_app.logger.debug('USER_ID: {}'.format(callback.user.id))
            current_app.logger.debug('CALLBACK_DATA: {}'.format(callback.data))

            if int(callback.user.id) == int(callback.data):
                callback.user.allow()
                callback.answerCallbackQuerry()
            else:
                callback.answerCallbackQuerry(allow=False)