import json
import keyword

import requests
from flask import current_app

import config


class FillAttributesMixin:
    def kwargs_to_attrs(self, **kwargs):
        for k, v in kwargs.items():
            if keyword.iskeyword(k):
                new_k = 'msg_' + k
                setattr(self, new_k, v)
            else:
                setattr(self, k, v)


class UpdateProcessor:
    def parse_update(self, **kwargs):
        for k,v in kwargs.items():
            if k == 'message':
                self.message = Message()
                self.message.kwargs_to_attrs(**v)
                self.parse_message()
                current_app.logger.debug("[MESSAGE]: {}, body: {}".format(self.message, vars(self.message)), extra={'uid': ''})
            #elif k = 'callback_query':
                #self.callback = CallBackQuery()
                #self.callback.kwargs_to_attrs(**v)
                #self.callback.message = Message(**v)
                

    def parse_message(self):
        if hasattr(self.message, 'msg_from'):
            self.user = User()
            self.user.kwargs_to_attrs(**self.message.msg_from)
            self.message.user = self.user
            current_app.logger.debug("[USER]: {}, body: {}".format(self.user, vars(self.user)), extra={'uid': ''})
        if hasattr(self.message, 'chat'):
            self.chat = Chat()
            self.chat.kwargs_to_attrs(**self.message.chat)  
            self.message.chat = self.chat
            current_app.logger.debug("[CHAT]: {}, body: {}".format(self.chat, vars(self.chat)), extra={'uid': ''})


class Bot(FillAttributesMixin, UpdateProcessor):
    def __init__(self, json_data):
        self.data = json_data
        current_app.logger.debug("[INCOMMING_UPDATE]: {}".format(self.data), extra={'uid': ''})
        #self.parse_and_fill()
        self.parse_update(**self.data)
        self.process_update()
        
    def process_update(self):
        if hasattr(self, 'message'):
            if hasattr(self.message, 'new_chat_members'):
                self.restrict_user()
    
    def restrict_user(self):
        data = {}
        reply_markup = {"inline_keyboard": [[{"text": "Войти", "callback_data": ""}]]}
        #callback_data = json.dumps({'user': self.user.id, 'first_name': self.user.first_name}, ensure_ascii=False)
        callback_data = json.dumps({'user': self.user.id}, ensure_ascii=False)
        reply_markup['inline_keyboard'][0][0]['callback_data'] = callback_data
        data['text'] = 'Привет, {}! Чтобы войти в чат - нажми кнопку "Войти"'.format(self.user.first_name)
        data['reply_markup'] = json.dumps(reply_markup, ensure_ascii=False)
        data['reply_to_message_id'] = self.message.message_id
        data['chat_id'] = self.chat.id
        r = requests.post(self.get_url("sendMessage"), data=data)
        current_app.logger.debug('[DATA_SEND_MESSAGE_METHOD]: {}'.format(data), extra={'uid': ''})
        current_app.logger.debug('[REQUEST_SEND_MESSAGE_METHOD]: {}'.format(r.url), extra={'uid': ''})
        current_app.logger.debug('[RESPONSE_SEND_MESSAGE_METHOD]: {}'.format(r.text), extra={'uid': ''})
        data = {'user_id': self.user.id, 'chat_id': self.chat.id}
        r = requests.post(self.get_url('restrictChatMember'), data=data)
        current_app.logger.debug('[DATA_RESTRICT_USER]: {}'.format(data), extra={'uid': ''})
        current_app.logger.debug('[REQUEST_RESTRICT_USER]: {}'.format(r.url), extra={'uid': ''})
        current_app.logger.debug(r.text, extra={'uid': ''})

    def alow_user(self):
        data = {'user_id': self.user.id,
                'can_send_messages': 'True',
                'can_send_media_messages': 'True',
                'can_send_other_messages': 'True',
                'can_add_web_page_previews': 'True',
                'chat_id': self.chat.id}
        r = requests.post(get_url('restrictChatMember'), data=data)
        current_app.logger.debug('[DATA_ALLOW_USER]: {}'.format(data), extra={'uid': ''})
        current_app.logger.debug('[REQUEST_ALLOW_USER]: {}'.format(r.url), extra={'uid': ''})
        current_app.logger.debug('[RESPONSE_ALLOW_USER] {}'.format(r.text), extra={'uid': ''})

    def get_url(self, method):
        return "https://api.telegram.org/bot{}/{}".format(config.token, method)

    def send_message(self):
        pass

            
class Message(FillAttributesMixin):
    pass

                
class Chat(FillAttributesMixin):
    pass

class User(FillAttributesMixin):
    pass

class CallBackQuery(FillAttributesMixin):
    pass