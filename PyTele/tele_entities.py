import json
import config
import requests
#import current_app.logger
import uuid
from flask import current_app


def get_url(method):
    return "https://api.telegram.org/bot{}/{}".format(config.token, method)


class Update:

    def __init__(self, update):
        self.update_id = update['update_id']


class Bot:

    def __init__(self, update, uid):
        self.uid = uid
        if 'message' in update.keys() and 'new_chat_members' in update['message'].keys():
            chat = Chat(update['message'])
            current_app.logger.debug('[CHAT_ATTRIBUTES]: {}'.format(chat.__dict__), extra={'uid': self.uid})
            message = Message(update['message'])
            user = User(update['message']['new_chat_members'][0], chat=chat, message=message)
            current_app.logger.debug('[USER ATTRIBUTES]: {}'.format(user.__dict__), extra={'uid': self.uid})
            self.restrict_user(user, message, chat)
            current_app.logger.debug('User ID: {}, Username: {}, Message: {}, Chat ID: {}, Chatname: {}'.format(user.id, user.username, message.message_id, chat.id, chat.username), extra={'uid': self.uid})
        elif 'callback_query' in update.keys():
            chat = Chat(update['callback_query']['message'])
            current_app.logger.debug('[CHAT_ATTRIBUTES]: {}'.format(chat.__dict__), extra={'uid': self.uid})
            message = Message(update['callback_query']['message'])
            user = User(update['callback_query']['from'])
            current_app.logger.debug('[USER ATTRIBUTES]: {}'.format(user.__dict__), extra={'uid': self.uid})
            callback = CallbackQuery(update, chat, message, self.uid, user)
            current_app.logger.debug('[CALLBACK_DATA]: {}'.format(callback.data), extra={'uid': self.uid})

            if int(user.id) == int(callback.data['user']):
                self.alow_user(user, chat)
                callback.answerCallbackQuerry()
                self.edit_welcome_message(message, chat, user)
            else:
                callback.answerCallbackQuerry(allow=False)

    def restrict_user(self, user, message, chat):
        data = {}
        reply_markup = {"inline_keyboard": [[{"text": "Войти", "callback_data": ""}]]}
        callback_data = json.dumps({'user': user.id, 'first_name': user.first_name}, ensure_ascii=False)
        reply_markup['inline_keyboard'][0][0]['callback_data'] = callback_data
        data['text'] = 'Привет, {}! Чтобы войти в чат - нажми кнопку "Войти"'.format(user.first_name)
        data['reply_markup'] = json.dumps(reply_markup, ensure_ascii=False)
        data['reply_to_message_id'] = message.message_id
        data['chat_id'] = chat.id
        r = requests.post(get_url("sendMessage"), data=data)
        current_app.logger.debug('[DATA_SEND_MESSAGE_METHOD]: {}'.format(data), extra={'uid': self.uid})
        current_app.logger.debug('[REQUEST_SEND_MESSAGE_METHOD]: {}'.format(r.url), extra={'uid': self.uid})
        current_app.logger.debug('[RESPONSE_SEND_MESSAGE_METHOD]: {}'.format(r.text), extra={'uid': self.uid})
        data = {'user_id': user.id, 'chat_id': chat.id}
        r = requests.post(get_url('restrictChatMember'), data=data)
        current_app.logger.debug('[DATA_RESTRICT_USER]: {}'.format(data), extra={'uid': self.uid})
        current_app.logger.debug('[REQUEST_RESTRICT_USER]: {}'.format(r.url), extra={'uid': self.uid})
        current_app.logger.debug(r.text, extra={'uid': self.uid})


    def alow_user(self, user, chat):
        data = {'user_id': user.id,
                'can_send_messages': 'True',
                'can_send_media_messages': 'True',
                'can_send_other_messages': 'True',
                'can_add_web_page_previews': 'True',
                'chat_id': chat.id}
        r = requests.post(get_url('restrictChatMember'), data=data)
        current_app.logger.debug('[DATA_ALLOW_USER]: {}'.format(data), extra={'uid': self.uid})
        current_app.logger.debug('[REQUEST_ALLOW_USER]: {}'.format(r.url), extra={'uid': self.uid})
        current_app.logger.debug('[RESPONSE_ALLOW_USER] {}'.format(r.text), extra={'uid': self.uid})

    def edit_welcome_message(self, message, chat, user):
        data = {'chat_id': chat.id,
                'message_id': message.message_id,
                'text': '{} прошел проверку. Поприветствуем!'.format(user.first_name)}
        r = requests.post(get_url('editMessageText'), data=data)
        current_app.logger.debug('[Message Edited]: {}'.format(r.text), extra={'uid': self.uid})



class Chat:
    def __init__(self, update):
        json_keys = update['chat'].keys()
        for i in json_keys:
            setattr(self, i, update['chat'][i])
        #current_app.logger.debug('[CHAT_ATTRIBUTES]: {}'.format(self.__dict__))



class Message(Update):

    def __init__(self, update):
        current_app.logger.debug("[MESSAGE_UPDATE] : {}".format(update), extra={'uid': ''})
        self.date = update['date']
        self.message_id = update['message_id']
        if 'text' in update.keys():
            self.text = update['text']


class User:
    def __init__(self, update, **kwargs):
        json_keys = update.keys()
        for i in json_keys:
            setattr(self, i, update[i])
        for i in kwargs:
            setattr(self, i, kwargs[i])
        #current_app.logger.debug('[USER ATTRIBUTES]: {}'.format(self.__dict__))




class CallbackQuery(Update):

    def __init__(self, update, chat, message, uid, user):
        self.id = update['callback_query']['id']
        self.chat_instanse = update['callback_query']['chat_instance']
        self.data = json.loads(update['callback_query']['data'])
        self.uid = uid
        self.first_name = user.first_name


    def answerCallbackQuerry(self, allow = True):
        data = {}
        data['callback_query_id'] = self.id
        if allow:
            data['text'] = 'Добро пожаловать!'
        else:
            data['text'] = 'Нажать клавишу может только {}!'.format(self.first_name)
        data['show_alert'] = 'True'
        r = requests.post(get_url('answerCallbackQuery'), data=data)
        current_app.logger.debug('[ANSWER_CALLBACK_QUERY]: {}'.format(r.text), extra={'uid': self.uid})
